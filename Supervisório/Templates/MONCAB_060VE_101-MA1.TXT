// Bearbeiter:    Kraus
// Freigabe:      Ja
// Datum:         23.08.2012
// Version:       1.0
// FC-Nummer:     
// Visu-Variante:
// Antrieb mit SINAMICS S120
// �nderung:      




TYPE;PW;Prozesswert;;;
  INT70;SOLL;0020E;;#;                       		 // Laufzeit SOLL
  INT72;IST;NNNNN;max. Positionierzeit (*0,1s);#;                        		 // Laufzeit IST
END_TYPE

TYPE;PW;Prozesswert;;;	
  INT246;SOLL;0020N;;;			// Override
  INT242;LOW;NNNNN;;;	     		// Untergrenze bei Eingabe
  INT244;HIGH;NNNNN;;;           	// Obergrenze bei Eingabe
END_TYPE

TYPE;PW;Prozesswert;;;	
  INT252;SOLL;0020N;;;			// Positionssatznummer im Einrichten
  INT248;LOW;NNNNN;;;	     		// Untergrenze bei Eingabe
  INT250;HIGH;NNNNN;;;           	// Obergrenze bei Eingabe
END_TYPE

TYPE;PW;Prozesswert;;;
  INT264;SOLL;NNNNN;Positionssatznummer;;             	// aktuelle Positionssatznummer SOLL
  INT260;IST;NNNNN;Positionssatznummer;;               	// aktuelle Positionssatznummer IST
END_TYPE

TYPE;PW;Prozesswert;;;			
  DINT170;Ist;NNNNN;Istposition;;	// Lageistwert
END_TYPE

TYPE;PW;Prozesswert;;;
  DINT266;SOLL;NNNNN;;;                 // Sollposition
END_TYPE

TYPE;PW;Prozesswert;;;
  DINT270;SOLL;NNNNN;;;           	// Sollgeschwindigkeit
END_TYPE

TYPE;PW;Prozesswert;;;
  INT274;SOLL;NNNNN;;;           	// Beschleunigung
END_TYPE

TYPE;PW;Prozesswert;;;
  INT276;SOLL;NNNNN;;;                	// Verz�gerung
END_TYPE


X143.0;PW;0020E;;;                	// Speicher Freigabe Positionieren
X143.3;PW;0020E;;;                	// Sollwerte �bernehmen im Einrichten
X142.4;PW;NNNNN;;;                	// Antrieb Ein
X116.6;PW;NNNNN;;;                	// Positionierung l�uft
X116.7;PW;NNNNN;;;                	// In Position

LEER1;PW;NNNNN;;;
LEER2;PW;NNNNN;------ Operating modes;;
X140.4;PW;0020E;;;                	// Betriebsart Tippen
X140.5;PW;0020E;;;                	// Betriebsart Justage

LEER3;PW;NNNNN;------ Encoder adjustment;;
X145.3;PW;NNNNN;;;                	// Freigabe Justage
X145.0;PW;0020E;;;                	// Anstoss Geberjustage
X145.1;PW;NNNNN;;;                	// Justage l�uft





TYPE;PW1;Prozesswert;;;			
  DINT170;Ist;NNNNN;;;			// Lageistwert
END_TYPE

TYPE;PW1;Prozesswert;;;			
  DINT174;Ist;NNNNN;;;			// Geschwindigkeitsistwert
END_TYPE

TYPE;PW1;Prozesswert;;;			
  INT178;Ist;NNNNN;;;			// St�rungscode
END_TYPE

TYPE;PW1;Prozesswert;;;			
  INT180;Ist;NNNNN;;;			// Warnungscode
END_TYPE

TYPE;PW1;Prozesswert;;;			
  INT192;Ist;NNNNN;;;			// Geschwindigkeitsoverride
END_TYPE

TYPE;PW1;Prozesswert;;;			
  DINT194;Ist;NNNNN;;;			// Positionssollwert
END_TYPE

TYPE;PW1;Prozesswert;;;			
  DINT198;Ist;NNNNN;;;			// Geschwindigkeitssollwert
END_TYPE

TYPE;PW1;Prozesswert;;;			
  INT202;Ist;NNNNN;;;			// Beschleunigungsoverride
END_TYPE

TYPE;PW1;Prozesswert;;;			
  INT204;Ist;NNNNN;;;			// Verz�gerungsoverride
END_TYPE

TYPE;PW;Prozesswert;;;
  BYTE540;SOLL;0020E;N� Produto;#;Produto                       		 // Produto
END_TYPE


TYPE;PW;Prozesswert;;;
  DINT300;SOLL;0020E;;#;Posi��o 1                       		 // Posi��o Inicial
END_TYPE

TYPE;PW;Prozesswert;;;
  DINT312;SOLL;0020E;;#;Posi��o 2                       		 // Posi��o Escape
END_TYPE

TYPE;PW;Prozesswert;;;
  DINT324;SOLL;0020E;;#;Posi��o 3                       		 // Posi��o Admissao
END_TYPE

TYPE;PW;Prozesswert;;;
  DINT336;SOLL;0020E;;#;Posi��o 4                       		 // Posi��o Top Brake
END_TYPE

LEER1;SO;NNNNN;------ Prozessdaten vom Umrichter;;
X160.0;SO;NNNNN;;;                	// Einschaltbereit
X160.1;SO;NNNNN;;;                	// Betriebsbereit
X160.2;SO;NNNNN;;;                	// Betrieb freigegeben
X160.3;SO;NNNNN;;;                	// St�rung wirksam
X160.7;SO;NNNNN;;;                	// Warnung wirksam
X161.2;SO;NNNNN;;;                	// Zielposition erreicht
X161.3;SO;NNNNN;;;                	// Referenzpunkt gesetzt
X164.2;SO;NNNNN;;;                	// Sollwert steht
X164.4;SO;NNNNN;;;                	// Achse f�hrt vorw�rts
X164.5;SO;NNNNN;;;                	// Achse f�hrt r�ckw�rts
X164.6;SO;NNNNN;;;                	// Softwareendschalter minus
X164.7;SO;NNNNN;;;                	// Softwareendschalter plus

LEER2;SO;NNNNN;------ Prozessdaten zum Umrichter;;
X184.0;SO;NNNNN;;;                	// Antrieb Ein
X184.1;SO;NNNNN;;;                	// AUS2 elektr. Halt
X184.2;SO;NNNNN;;;                	// AUS3 Schnellhalt
X184.3;SO;NNNNN;;;                	// Betrieb freigegeben
X184.4;SO;NNNNN;;;                	// EPOS Verfahrauftrag verwerfen (0-Signal)
X184.5;SO;NNNNN;;;                	// EPOS Zwischenhalt (0-Signal)
X184.7;SO;NNNNN;;;                	// St�rung Quittieren
X185.0;SO;NNNNN;;;                	// EPOS Tippen 1
X185.1;SO;NNNNN;;;                	// EPOS Tippen 2
X185.2;SO;NNNNN;;;                	// F�hrung durch PLC
X185.4;SO;NNNNN;;;                	// Bremse unbedingt �ffnen
X187.0;SO;NNNNN;;;                	// EPOS Sollwertdirektvorgabe/MDI Positioniertyp
X187.4;SO;NNNNN;;;                	// EPOS Sollwertdirektvorgabe/MDI �bernahmeart
X187.7;SO;NNNNN;;;                	// EPOS Sollwertdirektvorgabe/MDI Anwahl
X189.6;SO;NNNNN;;;                	// EPOS Softwareendschalter Aktivieren



